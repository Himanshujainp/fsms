package com.fuelstation.fsms.controller.employeecontroller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fuelstation.fsms.model.employeemodel.Employee;
import com.fuelstation.fsms.model.productmodel.Product;
import com.fuelstation.fsms.service.employeeservice.EmployeeAccountService;

@RestController
@RequestMapping("/employee")
public class EmloyeeHomeController {

	@Autowired
	EmployeeAccountService service;

	@GetMapping("/home")
	public ModelAndView getHomePage() {
		List<Product> prices = service.getProductPrices();
		ModelAndView mav = new ModelAndView("employee_home").addObject("prices", prices);
		return mav;
	}

	@RequestMapping("/viewAccount")
	public ModelAndView viewAccount(Principal userPrincipal) {
		String userName = userPrincipal.getName();
		Employee employee = service.findAccount(userName);
		ModelAndView mav = new ModelAndView("view_account").addObject("employee", employee);
		return mav;
	}

	@RequestMapping("/saveEmployee")
	public ModelAndView saveEmployee(Employee emp) {

		service.saveAccount(emp);
		ModelAndView mav = new ModelAndView("view_account").addObject("employee",
				service.findAccount(emp.getUserName()));
		return mav;
	}

	@GetMapping("/changePassword")
	public ModelAndView changePassword() {
		ModelAndView mav = new ModelAndView("change_password");
		return mav;
	}

	@PostMapping("/changePassword")
	public ModelAndView setNewPassword(HttpServletRequest req, Principal userPrincipal) {
		String oldPassword = req.getParameter("oldPassword");
		String newPassword = req.getParameter("newPassword");
		String confrmPassword = req.getParameter("confirmPassword");
		String userName = userPrincipal.getName();
		if (newPassword.equals(confrmPassword)) {
			boolean status = service.validatePassword(oldPassword, userName, newPassword);
			if (status == true) {
				return new ModelAndView("change_password").addObject("successMessage", "Password changed successfully");
			} else {
				return new ModelAndView("change_password").addObject("message", "Invalid Password ");
			}

		} else {
			return new ModelAndView("change_password").addObject("message", "Wrong Confirm Password ");
		}

	}
}
