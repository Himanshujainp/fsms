package com.fuelstation.fsms.controller.employeecontroller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fuelstation.fsms.model.productmodel.Sales;
import com.fuelstation.fsms.service.adminservice.ProductCrudOperationsService;
import com.fuelstation.fsms.service.adminservice.SalesReportService;
import com.fuelstation.fsms.service.employeeservice.SalesOperationsService;

@RestController
@RequestMapping("/employee")
public class SalesOperationController {

	@Autowired
	ProductCrudOperationsService service;

	@Autowired
	SalesOperationsService salesService;

	@Autowired
	SalesReportService salesReportService;

	@GetMapping("/addSales")
	public ModelAndView selectProductPage() {
		ModelAndView mav = new ModelAndView("add_sales");
		mav.addObject("prods", service.getAllProducts());

		mav.addObject("sales", new Sales());

		return mav;
	}

	@PostMapping("/addSales")
	public ModelAndView addSales(Sales sales, Principal principle) {

		String userName = principle.getName();
		if (salesService.saveSales(sales, userName)) {
			return new ModelAndView("add_sales").addObject("prods", service.getAllProducts())
					.addObject("successMessage", "Added successfully").addObject(new Sales());
		}
		return new ModelAndView("add_sales").addObject("message", " Product is out of stock");

	}

	@RequestMapping(path = "/editSales", method = RequestMethod.GET)
	public ModelAndView viewSale() {

		List<Sales> sales = salesReportService.viewSalesByDate();
		if (sales.size() > 0) {
			return new ModelAndView("edit_sales").addObject("sales", sales);

		} else {
			return new ModelAndView("edit_sales").addObject("Message", "No Sales done today");
		}
	}

	@RequestMapping("/updateSales")
	public ModelAndView editSalesById(Sales sale) throws Exception

	{
		String id = sale.getId();

		Sales sales = salesService.getSalesById(id);
		return new ModelAndView("update_sales").addObject("sales", sales).addObject("prods", service.getAllProducts());

	}

	@RequestMapping(path = "/createSales", method = RequestMethod.POST)
	public ModelAndView createOrUpdateSales(Sales sales) {
		salesService.createOrUpdateSales(sales);
		return new ModelAndView("redirect:/employee/editSales").addObject("employeedata",
				salesReportService.viewSalesByDate());
	}

	@RequestMapping(path = "/deleteSales", method = RequestMethod.POST)
	public ModelAndView deleteProduct(Sales sale) {
		String id = sale.getId();
		salesService.deleteSales(id);
		return new ModelAndView("redirect:/employee/editSales").addObject("employeedata",
				salesReportService.viewSalesByDate());
	}

	@RequestMapping(path = "/UpdateSalesBack")
	public ModelAndView back() {

		return new ModelAndView("redirect:/employee/editSales");
	}
}
