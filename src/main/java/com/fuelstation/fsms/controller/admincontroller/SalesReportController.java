package com.fuelstation.fsms.controller.admincontroller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fuelstation.fsms.model.employeemodel.PagerModel;
import com.fuelstation.fsms.model.productmodel.Sales;
import com.fuelstation.fsms.service.adminservice.SalesReportService;

@RestController
@RequestMapping("/admin")
public class SalesReportController {
	
	private static final int BUTTONS_TO_SHOW = 3;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 10;
	private static final int[] PAGE_SIZES = { 10, 25, 50 };
	
	@Autowired
	SalesReportService service;

	@RequestMapping(path = "/viewSales", method = RequestMethod.GET)
	public ModelAndView viewSale() {
		List<String> employees = service.viewEmployees();
		return new ModelAndView("view_sales").addObject("employees", employees);

	}

	@RequestMapping(path = "/viewSalesReport" )
	public ModelAndView viewSalesReport(HttpServletRequest req, @RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page) throws Exception {
		String fromdate=req.getParameter("fromdate");
		
		String todate = req.getParameter("todate");
		System.out.println(fromdate);
		System.out.println(todate);
		
		ModelAndView modelAndView = new ModelAndView("view_sales");
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		PageRequest pageable = PageRequest.of(evalPage, evalPageSize);

		Page<Sales> sales = service.viewSales(fromdate, todate, pageable);
		
		PagerModel pager = new PagerModel(sales.getTotalPages(), sales.getNumber(), BUTTONS_TO_SHOW);
		if (!sales.isEmpty()) {

			modelAndView.addObject("sales", sales).addObject("employees", service.viewEmployees());
			modelAndView.addObject("selectedPageSize", evalPageSize);
			// add page sizes
			modelAndView.addObject("pageSizes", PAGE_SIZES);
			// add pager
			modelAndView.addObject("pager", pager);
			return modelAndView;

		}
		return modelAndView.addObject("message", "There is no sales").addObject("employees", service.viewEmployees());
	}

	}
	
	
	
	
	
