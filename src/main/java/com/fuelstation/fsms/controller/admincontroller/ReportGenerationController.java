package com.fuelstation.fsms.controller.admincontroller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fuelstation.fsms.model.employeemodel.PagerModel;
import com.fuelstation.fsms.model.productmodel.DailyReport;
import com.fuelstation.fsms.model.productmodel.Price;
import com.fuelstation.fsms.model.productmodel.ProductPrices;
import com.fuelstation.fsms.service.adminservice.ReportGenerationService;

@RestController
@RequestMapping("/admin")
public class ReportGenerationController {

	private static final int BUTTONS_TO_SHOW = 3;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 10;
	private static final int[] PAGE_SIZES = { 10, 25, 50 };

	@Autowired
	ReportGenerationService service;

	@RequestMapping(path = "/dailyReport", method = RequestMethod.GET)
	public ModelAndView dailyReport() {
		List<String> products = service.getAllProducts();
		return new ModelAndView("daily_report").addObject("products", products);

	}

	@RequestMapping(path = "/viewDailyReport", method = RequestMethod.GET)
	public ModelAndView viewmonthlyReport(HttpServletRequest req, @RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page) {
		String month = req.getParameter("month");
		ModelAndView modelAndView = new ModelAndView("daily_report");
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		PageRequest pageable = PageRequest.of(evalPage, evalPageSize);
		Page<DailyReport> sale = service.dailyReport(month, pageable);
		PagerModel pager = new PagerModel(sale.getTotalPages(), sale.getNumber(), BUTTONS_TO_SHOW);
		if (sale.isEmpty()) {
			return modelAndView.addObject("month", month).addObject("message", "No Records exists!")
					.addObject("products", service.getAllProducts());
		}
		
		modelAndView.addObject("selectedPageSize", evalPageSize);
		// add page sizes
		modelAndView.addObject("pageSizes", PAGE_SIZES);
		// add pager
		modelAndView.addObject("pager", pager);
		return modelAndView.addObject("sale", sale).addObject("month", month).addObject("products",
				service.getAllProducts());
	}

	@RequestMapping(path = "/monthlyReport", method = RequestMethod.GET)
	public ModelAndView monthlyReport() {
		// String[] months = service.getAllMonths();
		return new ModelAndView("monthly_report").addObject("products", service.getAllProducts());

	}

	@RequestMapping(path = "/viewMonthlyReport")
	public ModelAndView viewMonthlyReport(HttpServletRequest req, @RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page) {

		String year = req.getParameter("month");
		ModelAndView modelAndView = new ModelAndView("monthly_report");
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		PageRequest pageable = PageRequest.of(evalPage, evalPageSize);
		Page<DailyReport> sale = service.getMonthlyReport(year, pageable);
		
		PagerModel pager = new PagerModel(sale.getTotalPages(), sale.getNumber(), BUTTONS_TO_SHOW);
		if (sale.isEmpty()) {
			return modelAndView.addObject("message", "No Records exists!").addObject("products",
					service.getAllProducts());

		}
		modelAndView.addObject("selectedPageSize", evalPageSize);
		// add page sizes
		modelAndView.addObject("pageSizes", PAGE_SIZES);
		// add pager
		modelAndView.addObject("pager", pager);
		return modelAndView.addObject("sale", sale).addObject("year", year).addObject("products",
				service.getAllProducts());

	}

	@RequestMapping(path = "/yearlyReport", method = RequestMethod.GET)
	public ModelAndView yearlyReport() {
		return new ModelAndView("yearly_report").addObject("products", service.getAllProducts());

	}

	@RequestMapping(path = "/viewYearlyReport")
	public ModelAndView viewYearlyReport(HttpServletRequest req, @RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page) {
		String year = req.getParameter("year");
		ModelAndView modelAndView = new ModelAndView("yearly_report");
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		PageRequest pageable = PageRequest.of(evalPage, evalPageSize);
		Page<DailyReport> sale = service.getYearlyReport(year, pageable);
		PagerModel pager = new PagerModel(sale.getTotalPages(), sale.getNumber(), BUTTONS_TO_SHOW);
		if (sale.isEmpty()) {
			return modelAndView.addObject("message", "No Records exists!").addObject("products",
					service.getAllProducts());

		}
		modelAndView.addObject("selectedPageSize", evalPageSize);
		// add page sizes
		modelAndView.addObject("pageSizes", PAGE_SIZES);
		// add pager
		modelAndView.addObject("pager", pager);
		return modelAndView.addObject("sale", sale).addObject("year", year).addObject("products",
				service.getAllProducts());

	}

	@RequestMapping(path = "/priceReport", method = RequestMethod.GET)
	public ModelAndView priceReport() {
		return new ModelAndView("price_report");
		// .addObject("products",service.getAllProducts());

	}

	@RequestMapping(path = "/viewPriceReport")
	public ModelAndView viewPriceReport(HttpServletRequest req, @RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page) {
		String variant = req.getParameter("product");
		ModelAndView modelAndView = new ModelAndView("price_report");
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		PageRequest pageable = PageRequest.of(evalPage, evalPageSize);
		System.out.println(variant);
		if (variant != null) {
			if (variant.equals("petroldiesel")) {
				Page<Price> petroldieselprices = service.viewPricesOfPetrolAndDiesel(variant, pageable);
				PagerModel pager = new PagerModel(petroldieselprices.getTotalPages(), petroldieselprices.getNumber(),
						BUTTONS_TO_SHOW);
				modelAndView.addObject("petroldieselprices", petroldieselprices);
				// add pager
				modelAndView.addObject("pager", pager);

			}
			if (variant.equals("other")) {

				Page<ProductPrices> prices = service.viewPrices(variant, pageable);
				PagerModel pager = new PagerModel(prices.getTotalPages(), prices.getNumber(), BUTTONS_TO_SHOW);
				modelAndView.addObject("prices", prices);
				// add pager
				modelAndView.addObject("pager", pager);
			}

		}
		modelAndView.addObject("selectedPageSize", evalPageSize);
		// add page sizes
		modelAndView.addObject("pageSizes", PAGE_SIZES);

		return modelAndView;
	}
}
