package com.fuelstation.fsms.controller.admincontroller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fuelstation.fsms.model.employeemodel.Employee;
import com.fuelstation.fsms.model.employeemodel.PagerModel;
import com.fuelstation.fsms.service.adminservice.EmployeeCrudOperationsService;
import com.fuelstation.fsms.service.adminservice.SalesReportService;

@RestController
@RequestMapping("/admin")
public class EmployeeOperationsController {
	
	private static final int BUTTONS_TO_SHOW = 3;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 10;
	private static final int[] PAGE_SIZES = { 10, 25, 50};
	
	@Autowired
	EmployeeCrudOperationsService service;

	@Autowired
	SalesReportService salesService;
	
	
	@GetMapping("/createNewEmployee")
	public ModelAndView getCreateEmployeePage() {
		ModelAndView mav = new ModelAndView("create_employee");
		mav.addObject(new Employee());
		return mav;
	}

	@PostMapping("/createNewEmployee")
	public ModelAndView createEmployee(@ModelAttribute("employee") Employee employee) {
		if (service.createOrUpdateEmployee(employee) == "success") {
			return new ModelAndView("create_employee").addObject("successMessage", "Added Employee Successfully")
					.addObject(new Employee());
		} else if (service.createOrUpdateEmployee(employee) == "failure") {
			return new ModelAndView("create_employee").addObject("message", "Employee already exists")
					.addObject(new Employee());
		} else
			return new ModelAndView("create_employee").addObject("message", "Error Creating Employee");

	}
	
	@RequestMapping(value = "/viewEmployee", method = RequestMethod.GET)
	public ModelAndView viewEmployee(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page) throws Exception {
		ModelAndView modelAndView = new ModelAndView("view_employee");
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		PageRequest pageable = PageRequest.of(evalPage, evalPageSize);
		Page<Employee> employeeList = service.viewEmployee(pageable);

		System.out.println("Employee list get total pages" + employeeList.getTotalPages() + "Employee list get number "
				+ employeeList.getNumber());
		PagerModel pager = new PagerModel(employeeList.getTotalPages(), employeeList.getNumber(), BUTTONS_TO_SHOW);
		
		modelAndView.addObject("employeeList", employeeList).addObject("employees", salesService.viewEmployees());;
		// evaluate page size
		modelAndView.addObject("selectedPageSize", evalPageSize);
		// add page sizes
		modelAndView.addObject("pageSizes", PAGE_SIZES);
		// add pager
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}


	@RequestMapping(value = "/back", method = RequestMethod.GET)
	public ModelAndView back() {
		return new ModelAndView("redirect:/admin/viewEmployee");

	}

	@RequestMapping("/deleteEmployee")
	public ModelAndView deleteUser(Employee employee) {
		String id = employee.getId();
		boolean status = service.deleteEmployee(id);
		if (status) {
			return new ModelAndView("redirect:/admin/viewEmployee");
		} else {
			return new ModelAndView("redirect:/admin/viewEmployee");
		}
	}

	@RequestMapping(path="/editEmployee", method=RequestMethod.POST)
	public ModelAndView editEmployeeById(Employee employee) throws Exception

	{
		String id = employee.getId();
		Employee entity = service.getEmployeeById(id);
		return new ModelAndView("update_employee").addObject("employee", entity);

	}

	@RequestMapping(path = "/createEmployee", method = RequestMethod.POST)
	public ModelAndView createOrUpdateEmployee(Employee employee) throws Exception {
	String status=	service.createOrUpdateEmployee(employee);
	if(status.equals("exists")) {
		return new ModelAndView("update_employee").addObject("employee", service.getEmployeeById(employee.getId())).addObject("message", "Email already Exists!");
	}
	return new ModelAndView("redirect:/admin/viewEmployee");
}
}