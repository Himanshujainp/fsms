package com.fuelstation.fsms.controller.admincontroller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fuelstation.fsms.model.employeemodel.PagerModel;
import com.fuelstation.fsms.model.productmodel.Product;
import com.fuelstation.fsms.service.adminservice.ProductCrudOperationsService;

@RestController
@RequestMapping("/admin")
public class ProductOperationsController {

	
	private static final int BUTTONS_TO_SHOW = 3;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10};
	
	@Autowired
	ProductCrudOperationsService service;

	@GetMapping("/addProduct")
	public ModelAndView getAddProductPage() {
		ModelAndView mav = new ModelAndView("add_product");
		mav.addObject(new Product());
		return mav;
	}

	@PostMapping("/addProduct")
	public ModelAndView addProduct(@ModelAttribute("product") Product product) {
		if (service.createOrUpdateProduct(product) == "success") {
			return new ModelAndView("add_product").addObject("successMessage", "Added Product Successfully")
					.addObject(new Product());
		} else if (service.createOrUpdateProduct(product) == "failure") {
			return new ModelAndView("add_product").addObject("message", " Product already Exists");
		}
		return new ModelAndView("add_product").addObject("message", "Error Creating Product");
	}


	
	@RequestMapping(value = "/viewProducts")
	public ModelAndView viewProduct(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page) throws Exception {
		ModelAndView modelAndView= new ModelAndView("view_product");
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		PageRequest pageable = PageRequest.of(evalPage, evalPageSize);
		
		Page<Product> productList = service.viewProducts(pageable);
		System.out.println("Employee list get total pages" + productList.getTotalPages() + "Employee list get number "
				+ productList.getNumber());
		PagerModel pager = new PagerModel(productList.getTotalPages(), productList.getNumber(), BUTTONS_TO_SHOW);
		
		modelAndView.addObject("products", productList).addObject("productList", service.getAllProducts());
		// evaluate page size
		modelAndView.addObject("selectedPageSize", evalPageSize);
		// add page sizes
		modelAndView.addObject("pageSizes", PAGE_SIZES);
		// add pager
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	
	@RequestMapping("/deleteProduct")
	public ModelAndView deleteProduct(Product product) {
		String id = product.getId();
		service.deleteProduct(id);
		return new ModelAndView("redirect:/admin/viewProducts");
	}

	@RequestMapping("/editProduct")
	public ModelAndView editProductById(Product product) throws Exception

	{
		String id = product.getId();
		Product entity = service.getProductById(id);
		return new ModelAndView("update_product").addObject("product", entity);

	}

	@RequestMapping(path = "/createProduct", method = RequestMethod.POST)
	public ModelAndView createOrUpdateProduct(Product product) {
		service.createOrUpdateProduct(product);
		return new ModelAndView("redirect:/admin/viewProducts");
	}

	@RequestMapping(path = "/UpdateProductsback")
	public ModelAndView back() {

		return new ModelAndView("redirect:/admin/viewProducts");
	}
}
