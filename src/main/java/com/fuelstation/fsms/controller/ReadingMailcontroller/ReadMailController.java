package com.fuelstation.fsms.controller.ReadingMailcontroller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fuelstation.fsms.service.Emailservice.MailReadService;

@RestController
@RequestMapping("/admin")
public class ReadMailController {

	@Autowired
	MailReadService service;

	@Scheduled(cron = "0 33 16 ? * *")
	public void readmail() {

		try {
			service.readMails();
		} catch (IOException e) {
			e.printStackTrace();
		}
		;
	}
}
