package com.fuelstation.fsms.converters;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.fuelstation.fsms.model.employeemodel.Employee;

@Component
public class EmployeeDetailsConverter {

	public UserDetails convertEmployeeDetails(Employee employee) {


		User user = (User) User.withDefaultPasswordEncoder().username(employee.getUserName())
				.password(employee.getPassword())
				.disabled(employee.isDisabled())
				.roles(employee.getRoles())
				.build();
System.out.println(employee.getUserName());

		return user;

	}
}
