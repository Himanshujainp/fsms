package com.fuelstation.fsms.model.employeemodel;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "employee_details")
public class Employee {

	@Id
	private String id;

	@Field(name = "username")

	private String userName;

	@Field(name = "password")
	private String password;

	@Field(name = "disabled")
	private boolean disabled;

	@Field(name = "roles_assigned")
	private String roles;

	@Indexed(unique=true)
	@Field(name = "email")
	private String email;

	@Field(name = "phone")
	private String phone;

	@Field(name = "salary")
	private double salary;

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	//
//	@Field(name = "account_not_expired")
//	private  boolean accountNonExpired;
//	
//	@Field(name = "account_not_locked")
//	private  boolean accountNonLocked;
//	
//	@Field(name = "credentials_not_expired")
//	private  boolean credentialsNonExpired;
//
//	public boolean isAccountNonExpired() {
//		return accountNonExpired;
//	}
//
//	public void setAccountNonExpired(boolean accountNonExpired) {
//		this.accountNonExpired = accountNonExpired;
//	}
//
//	public boolean isAccountNonLocked() {
//		return accountNonLocked;
//	}
//
//	public void setAccountNonLocked(boolean accountNonLocked) {
//		this.accountNonLocked = accountNonLocked;
//	}
//
//	public boolean isCredentialsNonExpired() {
//		return credentialsNonExpired;
//	}
//
//	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
//		this.credentialsNonExpired = credentialsNonExpired;
//	}
//
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

}
