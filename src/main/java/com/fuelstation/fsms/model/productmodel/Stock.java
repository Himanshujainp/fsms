package com.fuelstation.fsms.model.productmodel;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Field;

public class Stock {

	@Field(name = "available_quantity")
	private long quantity;

	@Field(name = "last_updated")
	private Date lastUpdated;

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
}
