package com.fuelstation.fsms.model.productmodel;

import org.springframework.data.mongodb.core.mapping.Field;

public class SalesProduct {

	@Field(name = "product_id")
	private String id;

	@Field(name = "product_name")
	private String name;

	@Field(name = "quantity_sold")
	private long quantity;

	@Field(name = "price")
	private double price;
	
	@Field(name = "total_price")
	private double totalPrice;

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/*
	 * /// Base price
	 * 
	 * @Field(name = "price") private Product baseprice;
	

	public Product getBaseprice() {
		return baseprice;
	}

	public void setBaseprice(Product baseprice) {
		this.baseprice = baseprice;
	}
 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
