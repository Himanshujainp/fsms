package com.fuelstation.fsms.model.productmodel;

public class DailyReport {
	private String name;
	private long totalQty;
	private double newTotalPrice;
	private String date;
	private double price;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public double getNewTotalPrice() {
		return newTotalPrice;
	}

	public void setNewTotalPrice(double newTotalPrice) {
		this.newTotalPrice = newTotalPrice;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(long totalQty) {
		this.totalQty = totalQty;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "DailyReport [name=" + name + ", totalQty=" + totalQty + ", newTotalPrice=" + newTotalPrice + ", date="
				+ date + ", price=" + price + "]";
	}

}
