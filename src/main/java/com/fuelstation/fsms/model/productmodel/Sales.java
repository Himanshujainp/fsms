package com.fuelstation.fsms.model.productmodel;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Sales {

	
	@Id
	private String id;

	@Field(name = "employee_id")
	private String employeeId;

	@Field(name = "sales_date")
	private String date;

	public void setDate(String date) {
		this.date = date;
	}

	@Field(name = "products_sold")
	private SalesProduct products;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SalesProduct getProducts() {
		return products;
	}

	public String getDate() {
		return date;
	}

	public void setProducts(SalesProduct products) {
		this.products = products;
	}
	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}


}
