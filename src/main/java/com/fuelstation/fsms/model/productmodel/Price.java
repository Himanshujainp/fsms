package com.fuelstation.fsms.model.productmodel;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Price {
	@Id
	private String id;

	@Field(name = "date")
	private String date;

	@Field(name = "petrol_price")
	private double petrolPrice;

	@Field(name = "diesel_price")
	private double dieselPrice;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getPetrolPrice() {
		return petrolPrice;
	}

	public void setPetrolPrice(double petrolPrice) {
		this.petrolPrice = petrolPrice;
	}

	public double getDieselPrice() {
		return dieselPrice;
	}

	public void setDieselPrice(double dieselPrice) {
		this.dieselPrice = dieselPrice;
	}

}
