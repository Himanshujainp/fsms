
package com.fuelstation.fsms.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.fuelstation.fsms.service.authenticationservice.CustomizeAuthenticationSuccessHandler;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	CustomizeAuthenticationSuccessHandler customizeAuthenticationSuccessHandler;
	// Setting up the Authentication

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}

	// Setting up the Authorization

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/admin/**").hasRole("ADMIN")
				// .antMatchers("/employee/home").hasRole("EMPLOYEE")
				.antMatchers("/employee/**").hasAnyRole("ADMIN", "EMPLOYEE")

				.antMatchers("/").permitAll().and().formLogin().successHandler(customizeAuthenticationSuccessHandler)
				.loginPage("/login").failureUrl("/loginfailure")

				.permitAll().and().logout()

				.logoutSuccessUrl("/logout").and().exceptionHandling();

		;
	}

//	}
// //Password Encoder // public PasswordEncoder getPasswordEncoder() { 
	// return(PasswordEncoder)User.withDefaultPasswordEncoder();
// }
	/*
	 * @Bean public AuthenticationFailureHandler
	 * customAuthenticationFailureHandler() { return new
	 * CustomAuthenticationFailureHandler(); }
	 */
}
