package com.fuelstation.fsms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FsmsApplication {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		SpringApplication.run(FsmsApplication.class, args);
	}

}