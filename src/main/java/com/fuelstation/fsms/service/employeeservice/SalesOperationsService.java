package com.fuelstation.fsms.service.employeeservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import com.fuelstation.fsms.model.employeemodel.Employee;
import com.fuelstation.fsms.model.productmodel.Product;
import com.fuelstation.fsms.model.productmodel.Sales;
import com.fuelstation.fsms.model.productmodel.SalesProduct;
import com.fuelstation.fsms.repository.EmployeeRepository;
import com.fuelstation.fsms.repository.ProductRepository;
import com.fuelstation.fsms.repository.SalesRepository;
import com.fuelstation.fsms.service.Emailservice.OutOfStockMailService;

@Service
public class SalesOperationsService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	ProductRepository productrepository;

	@Autowired
	SalesRepository repository;
	@Autowired
	MongoOperations mongoOperation;

	@Autowired
	OutOfStockMailService outOfStockMailService;

	public boolean saveSales(Sales sales, String userName) {
		try {

			Long salesQty = sales.getProducts().getQuantity(); // qty of sales
			String productName = sales.getProducts().getName();
			List<Product> product = productrepository.findByName(productName);
			double salesPrice = product.get(0).getCurrentPrice();
			double ttlPrice = salesPrice * salesQty;
			if (!product.isEmpty()) {

				Long productStock = product.get(0).getStock().getQuantity();
				if (productStock > salesQty) {

					Employee emp = employeeRepository.findByUserName(userName);
					sales.setEmployeeId(emp.getId());
					sales.getProducts().setPrice(salesPrice);
					sales.getProducts().setTotalPrice(ttlPrice);
					
					mongoOperation.save(sales);
					productStock = productStock - salesQty;
					/*
					 * if(qty<10) { outOfStockMailService.sendEmail(productName); }
					 */
					product.get(0).getStock().setQuantity(productStock);
					productrepository.save(product.get(0));
					return true;
				} else {
					outOfStockMailService.sendEmail(productName);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;

	}

	public Sales getSalesById(String id) throws Exception {

		Optional<Sales> sales = repository.findById(id);

		if (sales.isPresent()) {
			return sales.get();
		} else {
			throw new Exception("No  record exist for given id");
		}
	}

	public Sales createOrUpdateSales(Sales sales) {

		if (sales.getId() == null) {
			sales = repository.save(sales);

			return sales;
		} else {
			Optional<Sales> saleData = repository.findById(sales.getId());
			SalesProduct product = new SalesProduct();
			long salesQty = sales.getProducts().getQuantity();
			double salesPrice = sales.getProducts().getPrice();
			double salesTtl = salesPrice * salesQty;
			if (saleData.isPresent()) {
				Sales oldSales = saleData.get();
				product.setName(sales.getProducts().getName());
				product.setPrice(salesPrice);
				product.setQuantity(salesQty);
				product.setTotalPrice(salesTtl);
				oldSales.setProducts(product);
				oldSales = repository.save(oldSales);

				return oldSales;
			} else {
				sales = repository.save(sales);

				return sales;
			}

		}
	}

	public void deleteSales(String id) {

		repository.deleteById(id);
	}
}
