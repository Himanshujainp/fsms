
package com.fuelstation.fsms.service.authenticationservice;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fuelstation.fsms.converters.EmployeeDetailsConverter;
import com.fuelstation.fsms.model.employeemodel.Employee;
import com.fuelstation.fsms.repository.EmployeeRepository;

@Service
public class EmployeeDetailsAuthenticationService implements UserDetailsService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	EmployeeDetailsConverter converter;

	@Autowired
	private HttpServletRequest request;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		String mail = request.getParameter("email");

		Employee userDetails = employeeRepository.findByEmail(mail); 

		if (userDetails != null) {

			return converter.convertEmployeeDetails(userDetails);

		} else {
			return null;
		}

	}
}
