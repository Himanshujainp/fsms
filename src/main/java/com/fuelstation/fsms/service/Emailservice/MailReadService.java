package com.fuelstation.fsms.service.Emailservice;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fuelstation.fsms.model.productmodel.Price;
import com.fuelstation.fsms.model.productmodel.Product;
import com.fuelstation.fsms.repository.EmailRepository;
import com.fuelstation.fsms.repository.ProductRepository;

@Service
public class MailReadService {
	@Autowired
	EmailRepository repository;

	@Autowired
	ProductRepository productRepository;

	Price price = new Price();
	LocalDate today = LocalDate.now();

	public void readMails() throws IOException {

		Properties properties = new Properties();
//		properties.setProperty("mail.host", "imap.gmail.com");
//		properties.setProperty("mail.port", "995");
//		properties.setProperty("mail.transport.protocol", "imaps");
		String file="application.properties";
		InputStream inputStream= getClass().getClassLoader().getResourceAsStream(file);
		if(inputStream!=null) {
			properties.load(inputStream);
		}else {
			throw new FileNotFoundException(file +"not found");
		}
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(properties.getProperty("mail.username"), properties.getProperty("mail.password"));
			}
		});
		try {
			Store store = session.getStore("imaps");
			store.connect();
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			// Message messages[] = inbox.search(new FlagTerm(new Flags(Flag.SEEN), false));

			SearchTerm sender = new FromTerm(new InternetAddress("ashikahegde05@gmail.com"));

			Message[] messages = inbox.search(sender);
			System.out.println("Number of mails = " + messages.length);

			Message message = messages[messages.length - 1];
			Date date = message.getSentDate();

			SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
			String sentDate = DateFor.format(date);

			if (sentDate.equals(today.toString())) {

				Address[] from = message.getFrom();
				System.out.println("-------------------------------");
				System.out.println("Date : " + message.getSentDate());
				System.out.println("From : " + from[0]);
				System.out.println("Subject: " + message.getSubject());
				System.out.println("Content :");
				processMessageBody(message);
				System.out.println("--------------------------------");
			}

			inbox.close(true);
			store.close();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void processMessageBody(Message message) {
		try {
			Object content = message.getContent(); // check for string // then check for multipart
			if (content instanceof String) {
				System.out.println(content);

			} else if (content instanceof Multipart) {
				Multipart multiPart = (Multipart) content;

				procesMultiPart(multiPart);
			} else if (content instanceof InputStream) {
				InputStream inStream = (InputStream) content;
				int ch;
				while ((ch = inStream.read()) != -1) {
					System.out.write(ch);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void procesMultiPart(Multipart content) {
		try {
			List<String> priceList = new ArrayList<String>();
			int multiPartCount = content.getCount();
			for (int i = 0; i < multiPartCount; i++) {
				BodyPart bodyPart = content.getBodyPart(i);
				System.out.println();
				Object o;
				o = bodyPart.getContent();
				if (o instanceof String) {

					String[] s = ((String) o).split("\n");// split("=", );
					for (String string : s) {
						String[] s1 = string.split("=");
						System.out.println(s1[1]);
						priceList.add(s1[1]);

					}
					System.out.println("list0" + priceList.get(0));
					System.out.println("list1" + priceList.get(1));
					price.setDate(today.toString());
					price.setPetrolPrice(Double.valueOf(priceList.get(0)));
					price.setDieselPrice(Double.valueOf(priceList.get(1)));
					List<Product> productPetrol = productRepository.findByName("Petrol");
					List<Product> productDisel = productRepository.findByName("Disel");
					productPetrol.get(0).setCurrentPrice(Double.valueOf(priceList.get(0)));
					productDisel.get(0).setCurrentPrice(Double.valueOf(priceList.get(1)));
					productRepository.save(productPetrol.get(0));
					productRepository.save(productDisel.get(0));
					repository.save(price);
					System.out.println(o);
				} else if (o instanceof Multipart) {
					procesMultiPart((Multipart) o);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	/*
	 * private void SavePriceData(Price pricedata) { repository.save(pricedata); }
	 */

}
