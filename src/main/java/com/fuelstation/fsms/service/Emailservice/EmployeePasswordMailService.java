package com.fuelstation.fsms.service.Emailservice;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.stereotype.Service;


@Service
public class EmployeePasswordMailService {

	public void sendPassword(String email, String password) throws AddressException, MessagingException, IOException {

		
		Properties props = new Properties();
		String file="application.properties";
		
		InputStream inputStream= getClass().getClassLoader().getResourceAsStream(file);
		
		if(inputStream!= null) {
			props.load(inputStream);
			System.out.println(props.getProperty("mail.smtp.username")+" " +props.getProperty("mail.smtp.password"));
		}else {
			throw new FileNotFoundException(file+ "not found");
		}
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("mail.smtp.username"), props.getProperty("mail.smtp.password"));
			}
		});
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(email, false));

		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
		msg.setSubject("Get Your Key!!");
		msg.setContent("Get Your Key!!", "text/html");
		msg.setSentDate(new Date());

		MimeBodyPart messageBodyPart = new MimeBodyPart();

		String message = "Thank You  for Registring, Your Password is " + password;
		messageBodyPart.setContent(message, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		msg.setContent(multipart);
		Transport.send(msg);
	}

}
