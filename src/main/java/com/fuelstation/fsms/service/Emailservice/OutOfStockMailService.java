package com.fuelstation.fsms.service.Emailservice;

import java.io.IOException;

import java.util.Date;
import java.util.Properties;
import javax.mail.PasswordAuthentication;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.stereotype.Service;

@Service
public class OutOfStockMailService {

	public void sendEmail(String productName) throws AddressException, MessagingException, IOException {

		System.out.println("inside mail sending");
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("ashikahegde05@gmail.com", "11051997");
			}
		});
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("ashikajog@gmail.com", false));

		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("ashikajog@gmail.com"));
		msg.setSubject("Notification for Out of Stock");
		msg.setContent("Notification for Out of Stock", "text/html");
		msg.setSentDate(new Date());

		MimeBodyPart messageBodyPart = new MimeBodyPart();
		
		String message = productName + " is Out of Stock";
		messageBodyPart.setContent(message, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		// MimeBodyPart attachPart = new MimeBodyPart();

		// attachPart.attachFile("/var/tmp/image19.png");
		// multipart.addBodyPart(attachPart);
		msg.setContent(multipart);
		Transport.send(msg);
	}

	

}
