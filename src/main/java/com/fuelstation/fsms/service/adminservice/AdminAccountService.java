package com.fuelstation.fsms.service.adminservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fuelstation.fsms.model.employeemodel.Employee;
import com.fuelstation.fsms.repository.EmployeeRepository;

@Service
public class AdminAccountService {
	@Autowired
	EmployeeRepository repository;

	public boolean validatePassword(String oldPassword, String userName, String newPassword) {

		Employee emp = repository.findByUserName(userName);
		String password = emp.getPassword();
		if (oldPassword.equals(password)) {
			emp.setPassword(newPassword);
			repository.save(emp);
			return true;
		} else {
			return false;
		}
	}

	public Employee findAccount(String userName) {

		return repository.findByUserName(userName);
	}

	public void saveAccount(Employee emp) {
		Employee employee = new Employee();

		employee.setUserName(emp.getUserName());
		repository.save(emp);

	}
}
