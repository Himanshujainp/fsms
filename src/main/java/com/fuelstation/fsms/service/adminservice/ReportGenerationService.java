package com.fuelstation.fsms.service.adminservice;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.previousOperation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import com.fuelstation.fsms.model.productmodel.DailyReport;
import com.fuelstation.fsms.model.productmodel.Price;
import com.fuelstation.fsms.model.productmodel.ProductPrices;
import com.fuelstation.fsms.model.productmodel.Sales;
import com.fuelstation.fsms.repository.PriceRepository;
import com.fuelstation.fsms.repository.ProductPricesRepository;
import com.fuelstation.fsms.repository.ReportRepository;

@Service
public class ReportGenerationService {

	@Autowired
	ReportRepository repository;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	ProductPricesRepository productPriceRepository;

	@Autowired
	PriceRepository priceRepository;

	@Autowired
	ProductCrudOperationsService productservice;

	public Page<DailyReport> dailyReport(String month, Pageable pageable) {

		Query query = new Query().with(pageable);
		Aggregation aggregation = newAggregation(

				match(Criteria.where("date").regex(".*" + month + ".*")),
				group("date", "products.name").sum("products.quantity").as("totalQty").sum("products.totalPrice")
						.as("newTotalPrice").addToSet("date").as("date").addToSet("products.name").as("name")
						.addToSet("products.price").as("price"),

				sort(Sort.Direction.ASC, previousOperation(), "date")

		);

		AggregationResults<DailyReport> groupResults = mongoTemplate.aggregate(aggregation, Sales.class,
				DailyReport.class);

		List<DailyReport> salesReport = groupResults.getMappedResults();

		return PageableExecutionUtils.getPage(salesReport, pageable,
				() -> mongoTemplate.count(Query.of(query).limit(-1).skip(-1), DailyReport.class));

	}

	/*
	 * public Page<Sales> dailyReport(PageRequest pageable, String month) { // TODO
	 * Auto-generated method stub return null; }
	 */

	public Page<DailyReport> getMonthlyReport(String year, Pageable pageable) {
		Query query = new Query().with(pageable);
		Aggregation aggregation = newAggregation(

				match(Criteria.where("date").regex(".*" + year + ".*")),
				group("products.name").sum("products.quantity").as("totalQty").sum("products.totalPrice")
						.as("newTotalPrice").addToSet("products.name").as("name").addToSet("products.price")
						.as("price"),

				sort(Sort.Direction.ASC, previousOperation(), "products.name")

		);
		AggregationResults<DailyReport> groupResults = mongoTemplate.aggregate(aggregation, Sales.class,
				DailyReport.class);

		List<DailyReport> salesReport = groupResults.getMappedResults();

		return PageableExecutionUtils.getPage(salesReport, pageable,
				() -> mongoTemplate.count(Query.of(query).limit(-1).skip(-1), DailyReport.class));

	}

	public Page<DailyReport> getYearlyReport(String year,Pageable pageable) {
		Query query = new Query().with(pageable);
		Aggregation aggregation = newAggregation(

				match(Criteria.where("date").regex(".*" + year + ".*")),
				group("products.name").sum("products.quantity").as("totalQty").sum("products.totalPrice")
						.as("newTotalPrice").addToSet("products.name").as("name").addToSet("products.price")
						.as("price"),

				sort(Sort.Direction.ASC, previousOperation(), "products.name")

		);
		AggregationResults<DailyReport> groupResults = mongoTemplate.aggregate(aggregation, Sales.class,
				DailyReport.class);

		List<DailyReport> salesReport = groupResults.getMappedResults();

		return PageableExecutionUtils.getPage(salesReport, pageable,
				() -> mongoTemplate.count(Query.of(query).limit(-1).skip(-1), DailyReport.class));
	}

	public List<String> getAllProducts() {
		return productservice.getAllProducts();

	}

	public Page<Price> viewPricesOfPetrolAndDiesel(String varian,Pageable pageable) {
		Page<Price> prices = priceRepository.findAll(pageable);
		return prices;
	}

	public Page<ProductPrices> viewPrices(String variant,Pageable pageable) {

		Page<ProductPrices> prices = productPriceRepository.findAll(pageable);

		return prices;
	}
}
