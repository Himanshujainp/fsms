package com.fuelstation.fsms.service.adminservice;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fuelstation.fsms.model.employeemodel.Employee;
import com.fuelstation.fsms.repository.EmployeeRepository;
import com.fuelstation.fsms.service.Emailservice.EmployeePasswordMailService;

@Service
public class EmployeeCrudOperationsService {

	@Autowired
	EmployeeRepository repository;
	@Autowired
	EmployeePasswordMailService passwordMailService;

	public String getRandomPassword() {
		Random r = new java.util.Random();
		String s = Long.toString(r.nextLong() & Long.MAX_VALUE, 36);

		return s;
	}

	
	public Page<Employee> viewEmployee(Pageable pageable) {
        return repository.findAll(pageable);
    }
	
	
	public boolean deleteEmployee(String id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public Employee getEmployeeById(String id) throws Exception {
		Optional<Employee> employee = repository.findById(id);

		if (employee.isPresent()) {
			return employee.get();
		} else {
			throw new Exception("No employee record exist for given id");
		}
	}

	public String createOrUpdateEmployee(Employee newEmployee) {
		try {
			if (newEmployee.getId() == null) {
				if (!(repository.existsByEmail(newEmployee.getEmail()))) {
					newEmployee.setPassword(getRandomPassword());
					repository.save(newEmployee);
					passwordMailService.sendPassword(newEmployee.getEmail(), newEmployee.getPassword());
					return "success";
				} else {
					return "failure";
				}
			} else {
				Optional<Employee> employee = repository.findById(newEmployee.getId());

				if (employee.isPresent()) {
					Employee newEntity = employee.get();
					newEntity.setUserName(newEmployee.getUserName());
					newEntity.setRoles(newEmployee.getRoles());

					newEntity.setEmail(newEmployee.getEmail());
					newEntity.setPhone(newEmployee.getPhone());
					newEntity.setSalary(newEmployee.getSalary());
					
						repository.save(newEntity);
					
					return "success";
				} else {
					repository.save(newEmployee);

					return "success";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
}
