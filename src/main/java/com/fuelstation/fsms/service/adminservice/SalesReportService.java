package com.fuelstation.fsms.service.adminservice;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fuelstation.fsms.model.employeemodel.Employee;
import com.fuelstation.fsms.model.productmodel.Sales;
import com.fuelstation.fsms.repository.EmployeeRepository;
import com.fuelstation.fsms.repository.SalesRepository;

@Service
public class SalesReportService {
	@Autowired
	SalesRepository repository;

	@Autowired
	MongoOperations mongoOperation;

	@Autowired
	EmployeeRepository employeeRepository;

	public List<Sales> viewSales(String username) {

		Employee emp = employeeRepository.findByUserName(username);
		String id = emp.getId();

		return repository.findByTheSalesEmployeeId(id);

	}


	public List<Sales> viewSalesByDate() {
		LocalDate localDate = LocalDate.now();
		String date = localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

		return repository.findByDate(date);

	}

	public List<String> viewEmployees() {
		Query query = new Query();
		query.fields().include("username");
		List<String> employees = mongoOperation.find(query, Employee.class).stream().map(p -> p.getUserName())
				.collect(Collectors.toList());
		return employees;
	}

	public Page<Sales> viewSales(String fromdate, String todate, Pageable pageable) {
		Page<Sales> sales = null;
		

		if (todate != "" && fromdate != "") {
	
			sales = repository.findByDateBetween(fromdate, todate, pageable);
			for (Sales sale : sales) {
				if (sale.getEmployeeId() != null) {
					Optional<Employee> emp = employeeRepository.findById(sale.getEmployeeId());
					String name = emp.get().getUserName();
					sale.setEmployeeId(name);
				}
			}
		}

		if (todate == "") {
			
			sales = repository.findByDateGreaterThan(fromdate, pageable);
			for (Sales sale : sales) {
				if (sale.getEmployeeId() != null) {
				Optional<Employee> emp = employeeRepository.findById(sale.getEmployeeId());
				String name = emp.get().getUserName();
				sale.setEmployeeId(name);
				}
			}
		}

		return sales;
	}
}
