package com.fuelstation.fsms.service.adminservice;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fuelstation.fsms.model.productmodel.Product;
import com.fuelstation.fsms.model.productmodel.ProductPrices;
import com.fuelstation.fsms.repository.EmployeeRepository;
import com.fuelstation.fsms.repository.ProductPricesRepository;
import com.fuelstation.fsms.repository.ProductRepository;
import com.fuelstation.fsms.service.Emailservice.OutOfStockMailService;

@Service
public class ProductCrudOperationsService {
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	ProductPricesRepository productPricesRepository;

	@Autowired
	ProductRepository repository;

	@Autowired
	MongoOperations mongoOperation;

	@Autowired
	OutOfStockMailService outOfStockMailService;

	public List<String> getAllProducts() {
		Query query = new Query();
		query.fields().include("product_name");
		List<String> productsList = mongoOperation.find(query, Product.class).stream().map(p -> p.getName())
				.collect(Collectors.toList());
		return productsList;
	}

//	public List<Product> viewProducts() {
//
//		return repository.findAll();
//	}
	
	public Page<Product> viewProducts(Pageable pageable) {
        return repository.findAll(pageable);
    }

	public boolean deleteProduct(String id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Product getProductById(String id) throws Exception {
		
		Optional<Product> product = repository.findById(id);

		if (product.isPresent()) {
			return product.get();
		} else {
			throw new Exception("No product record exist for given id");
		}
	}


	public String createOrUpdateProduct(Product entity) {
		
		try {
			if (entity.getId() == null) {
				if (!(repository.existsByName(entity.getName()))) {
					repository.save(entity);

					return "success";
				} else {
					return "failure";
				}
			} else {
				Optional<Product> product = repository.findById(entity.getId());
				LocalDate date=LocalDate.now();
				
				ProductPrices productPrices=new ProductPrices();
				if (product.isPresent()) {
					
					Product newEntity = product.get();
					newEntity.setName(entity.getName());
					newEntity.setCurrentPrice(entity.getCurrentPrice());

					newEntity.setStock(entity.getStock());

					repository.save(newEntity);
					productPrices.setProduct_id(newEntity.getId());
					productPrices.setName(newEntity.getName());
					productPrices.setCurrentPrice(newEntity.getCurrentPrice());
					productPrices.setDate(date.toString());
					productPricesRepository.save(productPrices);
					return "success";
				} else {
					repository.save(entity);

					return "success";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

}
