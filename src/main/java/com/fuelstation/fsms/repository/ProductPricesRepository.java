package com.fuelstation.fsms.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fuelstation.fsms.model.productmodel.ProductPrices;


public interface ProductPricesRepository extends MongoRepository<ProductPrices, String> {

	List<ProductPrices> findByDateBetween(String fromdate, String todate);

	List<ProductPrices> findByDateGreaterThan(String fromdate);

}
