package com.fuelstation.fsms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.fuelstation.fsms.model.productmodel.Sales;

@Repository
public interface SalesRepository extends MongoRepository<Sales, String> {

	List<Sales> findByDate(String date);

	Optional<Sales> findByEmployeeId(String id);

	Optional<Sales> findAllByEmployeeId(String id);
	
	@Query("{ 'employeeId' : ?0 }")
	  List<Sales> findByTheSalesEmployeeId(String employeeId);
	

	Page<Sales> findByDateBetween(String fromdate, String todate,Pageable pageable);

	Page<Sales> findByDateGreaterThan(String fromdate,Pageable pageable);

	

//	List<Sales> findByDateBetween(String fromdate, String todate);
//
//	List<Sales> findByDateGreaterThan(String fromdate);
	
//	Page<Sales> findByEmployeeId(Pageable pageable);


}
