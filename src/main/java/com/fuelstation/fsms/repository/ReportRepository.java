package com.fuelstation.fsms.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fuelstation.fsms.model.productmodel.Sales;

public interface ReportRepository extends MongoRepository<Sales, String> {
	List<Sales> findByDateStartingWith(String month);

	// List<Product> findByCurrentPrice();
}
