package com.fuelstation.fsms.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fuelstation.fsms.model.productmodel.Price;

public interface PriceRepository extends MongoRepository<Price, String> {


}
