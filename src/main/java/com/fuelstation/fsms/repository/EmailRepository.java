package com.fuelstation.fsms.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fuelstation.fsms.model.productmodel.Price;

public interface EmailRepository extends MongoRepository<Price, String> {

	Price findTop1ByOrderByDateDesc();

}
