package com.fuelstation.fsms.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.fuelstation.fsms.model.productmodel.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

	// List<Product> findAll(Query query, Class<Product> class1);
	List<Product> findByName(String name);
	Page<Product> findAll(Pageable pageable);
	boolean existsByName(String name);

	

}
