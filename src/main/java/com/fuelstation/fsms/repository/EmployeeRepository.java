package com.fuelstation.fsms.repository;



import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.fuelstation.fsms.model.employeemodel.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, String>{

	public Employee findByUserName(String username);
	
	Page<Employee> findAll(Pageable pageable);
	public List<Employee> findAll();
	public void deleteById(String id);

	Employee findByEmail(String email);
	 Employee findByRoles(String role);
	  int countByEmail(String mail);

	public boolean existsByEmail(String email);
	
}
